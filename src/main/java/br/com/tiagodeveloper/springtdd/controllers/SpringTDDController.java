package br.com.tiagodeveloper.springtdd.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tiagodeveloper.springtdd.models.Pessoa;
import br.com.tiagodeveloper.springtdd.models.PessoaRepository;

@RestController
public class SpringTDDController {
    
    @Autowired
    private PessoaRepository pessoaRepository;
    
    @GetMapping(value="/pessoas")
    public List<Pessoa> getPessoas() {
        return this.pessoaRepository.findAll();
    }
    @PutMapping(value="/pessoas/{id}")
    public Pessoa putPessoas(@PathVariable Integer id, @ModelAttribute Pessoa pessoa) {
        return this.pessoaRepository.save(pessoa);
    }
    
}
