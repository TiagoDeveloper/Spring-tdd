package br.com.tiagodeveloper.springtdd.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
//@RepositoryRestResource(collectionResourceRel="pessoas", path="pessoas")
public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

}
