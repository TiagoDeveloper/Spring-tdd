package br.com.tiagodeveloper.springtdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTDDApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringTDDApplication.class, args);
	}
}