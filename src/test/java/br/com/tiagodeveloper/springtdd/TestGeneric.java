package br.com.tiagodeveloper.springtdd;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@Component
public class TestGeneric<T> {
    
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    
    private MockMvc mockMvc;
    
    public void getListTest(String path, T t) throws Exception{
        LOGGER.info("Iniciando o teste getList do path {}", path);
        this.mockMvc = MockMvcBuilders.standaloneSetup(t).build();
        this.mockMvc.perform(MockMvcRequestBuilders.get(path))
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$").isArray());
        LOGGER.info("Fim do teste getList");
    }
    public void putTest(String path, T t,Object param) throws Exception{
        LOGGER.info("Iniciando o teste PUT do path {}", path);
        this.mockMvc = MockMvcBuilders.standaloneSetup(t).build();
        this.mockMvc.perform(MockMvcRequestBuilders.put(path)
                .flashAttr("pessoa", param)
                
        )
        .andExpect(MockMvcResultMatchers.status().isOk())
        .andExpect(jsonPath("$.id",is(1)));
        LOGGER.info("Fim do teste PUT");
    }
    

}
