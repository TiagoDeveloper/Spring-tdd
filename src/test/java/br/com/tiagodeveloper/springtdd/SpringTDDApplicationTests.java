package br.com.tiagodeveloper.springtdd;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.tiagodeveloper.springtdd.controllers.SpringTDDController;
import br.com.tiagodeveloper.springtdd.models.Pessoa;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SpringTDDApplicationTests {

	@Autowired
	@SuppressWarnings("unused")
	private TestRestTemplate restTemplate;

	@Autowired
	private TestGeneric<SpringTDDController> testGeneric;

	@SuppressWarnings("unused")
    private MockMvc mockMvc;
	
	@Autowired
	private SpringTDDController springTDDController;
	
	@Test
	public void getListTest() throws Exception {
	    testGeneric.getListTest("/pessoas",springTDDController);
	}
	@Test
	public void putTest() throws Exception {
	    testGeneric.putTest("/pessoas/"+1,springTDDController,new Pessoa(1,"tiago",29));
	}
	
	@Test
	public void getPessoasResponse() {
//	    String body = this.restTemplate.getForObject("/pessoas", String.class);
	}
}
